#!/bin/sh
echo ""
echo "Starting Docker-Linux ... "
echo ""
echo "[$(date "+%G/%m/%d %H:%M:%S")] Initalizing MySQL Database ..." 

echo "[$(date "+%G/%m/%d %H:%M:%S")] Checking MySQL socket ..."
if [ -d "/run/mysqld" ]; then
	echo "[$(date "+%G/%m/%d %H:%M:%S")] /run/mysqld already present !"
    echo "[$(date "+%G/%m/%d %H:%M:%S")] Changing privilege for /run/mysqld ..."
	chown -R mysql:mysql /run/mysqld
else
	echo "[$(date "+%G/%m/%d %H:%M:%S")] /run/mysqld not found, creating...."
	mkdir -p /run/mysqld
    echo "[$(date "+%G/%m/%d %H:%M:%S")] Changing privilege for /run/mysqld ..."
	chown -R mysql:mysql /run/mysqld
fi

echo "[$(date "+%G/%m/%d %H:%M:%S")] Checking MySQL directory ..." 
if [ -d "/var/lib/mysql/mysql" ]; then
	echo "[$(date "+%G/%m/%d %H:%M:%S")] MySQL directory already present !"
    echo "[$(date "+%G/%m/%d %H:%M:%S")] Changing privilege for /var/lib/mysql ..."
	chown -R mysql:mysql /var/lib/mysql
else
    echo "[$(date "+%G/%m/%d %H:%M:%S")] MySQL data directory not found !"
    echo "[$(date "+%G/%m/%d %H:%M:%S")] Changing privilege for /var/lib/mysql ..."
	chown -R mysql:mysql /var/lib/mysql
    echo "[$(date "+%G/%m/%d %H:%M:%S")] MySQL data directory not found, creating initial database ..."
    echo "[$(date "+%G/%m/%d %H:%M:%S")] Please wait, this may take a while ..."
	mysql_install_db --user=mysql >/dev/null 2>&1
	
	# MySQL ROOT Password
	echo "[$(date "+%G/%m/%d %H:%M:%S")] Checking MySQL Root Password ..." 
	if [ "$MYSQL_ROOT_PASSWORD" = "" ]; then
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Not finding any paramters about MySQL Root Password !"
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Generating MySQL Root Password ..."
		MYSQL_ROOT_PASSWORD="$(pwgen 16 1)"
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Generate Success !"
		echo "[$(date "+%G/%m/%d %H:%M:%S")] MySQL root Password is : $MYSQL_ROOT_PASSWORD"
	else
	    echo "[$(date "+%G/%m/%d %H:%M:%S")] Existing MySQL Root Password found : $MYSQL_ROOT_PASSWORD"
	fi

    # MySQL Database Name
	echo "[$(date "+%G/%m/%d %H:%M:%S")] Checking MySQL Database Name ..." 
	if [ "$MYSQL_DATABASE" = "" ]; then
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Not finding any paramters about MySQL Database !"
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Generating MySQL Database Name ..."
		MYSQL_DATABASE="cloudreve_db_$(pwgen 8 1)"
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Generate Success !"
		echo "[$(date "+%G/%m/%d %H:%M:%S")] MySQL Database name is : $MYSQL_DATABASE"
	else
	    MYSQL_DATABASE=${MYSQL_DATABASE:-""}
	    echo "[$(date "+%G/%m/%d %H:%M:%S")] Existing MySQL Database Name found : $MYSQL_DATABASE"
	fi

    # MySQL Username
	echo "[$(date "+%G/%m/%d %H:%M:%S")] Checking MySQL Username ..." 
	if [ "$MYSQL_USER" = "" ]; then
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Not finding any paramters about MySQL Username !"
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Generating MySQL Username ..."
		MYSQL_USER="cloudreve_user_$(pwgen 8 1)"
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Generate Success !"
		echo "[$(date "+%G/%m/%d %H:%M:%S")] MySQL Username is : $MYSQL_USER"
	else
	    MYSQL_USER=${MYSQL_USER:-""}
	    echo "[$(date "+%G/%m/%d %H:%M:%S")] Existing MySQL Username found : $MYSQL_USER"
	fi

	# MySQL User Password
	echo "[$(date "+%G/%m/%d %H:%M:%S")] Checking MySQL User Password ..." 
	if [ "$MYSQL_PASSWORD" = "" ]; then
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Not finding any paramters about User Password !"
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Generating MySQL User Password ..."
		MYSQL_PASSWORD="$(pwgen 16 1)"
        echo "[$(date "+%G/%m/%d %H:%M:%S")] Generate Success !"
		echo "[$(date "+%G/%m/%d %H:%M:%S")] MySQL User Password is : $MYSQL_PASSWORD"
	else
	    MYSQL_PASSWORD=${MYSQL_PASSWORD:-""}
	    echo "[$(date "+%G/%m/%d %H:%M:%S")] Existing MySQL User Password found : $MYSQL_PASSWORD"
	fi

echo "[$(date "+%G/%m/%d %H:%M:%S")] Initializing MySQL Database ..." 

	tfile=`mktemp`
	if [ ! -f "$tfile" ]; then
	    return 1
	fi

	cat << EOF > $tfile
USE mysql;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' identified by '$MYSQL_ROOT_PASSWORD' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' identified by '$MYSQL_ROOT_PASSWORD' WITH GRANT OPTION;
UPDATE user SET password=PASSWORD("$MYSQL_ROOT_PASSWORD") WHERE user='root' AND host='localhost';
EOF

	if [ "$MYSQL_DATABASE" != "" ]; then
	    echo "[$(date "+%G/%m/%d %H:%M:%S")] Creating database: $MYSQL_DATABASE"
	    echo "CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> $tfile
	    if [ "$MYSQL_USER" != "" ]; then
		echo "[$(date "+%G/%m/%d %H:%M:%S")] Creating user: $MYSQL_USER with password $MYSQL_PASSWORD"
		echo "grant all privileges on $MYSQL_DATABASE.* to $MYSQL_USER@localhost identified by '$MYSQL_PASSWORD';" >> $tfile
		echo "FLUSH PRIVILEGES;" >> $tfile
	    fi
	fi
	echo "[$(date "+%G/%m/%d %H:%M:%S")] Initializing MySQL ..." 
	/usr/bin/mysqld --user=mysql --bootstrap --verbose=0 < $tfile >/dev/null 2>&1
	rm -f $tfile
fi

echo "[$(date "+%G/%m/%d %H:%M:%S")] Starting MariaDB (MySQL) in deamon mode ..."
exec nohup /usr/bin/mysqld_safe --user=mysql >/dev/null 2>&1 &
echo "[$(date "+%G/%m/%d %H:%M:%S")] Starting PHP 7 ..."
exec nohup php-fpm7 -F >/dev/null 2>&1 &
echo "[$(date "+%G/%m/%d %H:%M:%S")] Starting Nginx ..."
echo "[$(date "+%G/%m/%d %H:%M:%S")] Finish Starting Docker-LNMP Enviroment !"
nginx -g 'daemon off;'